# CuteDialog

Replace of Zenity, Yad or KDialog in pure Qt, following principle "less is more". Still almost fully compatible with Zenity and Yad. Originally writen for Steam install script, but I'll attend to add more features. It already support color dialog, progress bar, custom lables and buttons, title change.

This program is part of [Flitter](https://gitlab.com/flitter-tools), all informations can be found in [this repository](https://gitlab.com/flitter-tools/General).

# License

See LICENSE file in repository root.
