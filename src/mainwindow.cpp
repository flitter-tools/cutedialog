#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFutureWatcher>
#include <QTimer>
#include <QStyle>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->progressBar->setHidden(true);
    ui->label->setHidden(true);
    ui->btn_close->setHidden(true);
    ui->la_slider_2->setHidden(true);
    ui->lineEdit->setHidden(true);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setupWindow(int resize, bool centered)
{
    if (resize == 0)
    {
        this->layout()->setSizeConstraint(QLayout::SetFixedSize);
    }
    else
    {
        this->setFixedWidth(resize);
        this->setFixedHeight(117); // do not allow dialog change height
    }

    if (centered)
    {
        this->setGeometry(
            QStyle::alignedRect(
                Qt::LeftToRight,
                Qt::AlignCenter,
                this->size(),
                qApp->desktop()->availableGeometry()
            )
        );
    }
}

void MainWindow::Progressbar()
{
    ui->progressBar->setHidden(false);
    ui->bb_default->setHidden(true);
    ui->label->setHidden(false);
    ui->btn_close->setHidden(nocancel);

    QtConcurrent::run(this, &MainWindow::readstdin);
    if (autoclose)
        connect(this, &MainWindow::finishReadStdin, this, &MainWindow::permanentexit);
}

void MainWindow::ScaleDialog()
{
    ui->bb_default->setHidden(nocancel);
    ui->la_slider_2->setHidden(false);

    connect(ui->horizontalSlider, &QSlider::valueChanged, this, &MainWindow::sliderValueChange);
}

void MainWindow::EntryDialog()
{
    ui->bb_default->setHidden(nocancel);
    ui->lineEdit->setHidden(false);
}

void MainWindow::sliderValueChange(int value)
{
    ui->scale_text->setText(QString::number(value));
}

void MainWindow::AutoClose()
{
    autoclose = true;
}

void MainWindow::NoCancel()
{
    nocancel = true;
}

void MainWindow::SetTitle(const QString &title)
{
    this->SetTitle(title);
}

void MainWindow::SetText(const QString &text)
{
    ui->label->setHidden(false);
    ui->label->setText(text);
}

void MainWindow::AddCustomButtons(const QStringList &buttons)
{
    ui->bb_default->setHidden(true);

    for (QString item : buttons)
    {
        QStringList nitem = item.split(":");
        QString nlabel = nitem.at(0);

        if (nlabel.size() >= 35)
        {
            nlabel.resize(35);
            nlabel += "...";
        }

        fbutton *button = new fbutton(nlabel);
        int ret = 0;

        if (nitem.count() > 1)
        {
            QString tmp = nitem.at(1);
            ret = tmp.toInt();

            // protect against overflow
            if (ret > 255)
                ret = 255;
            else if (ret < -255)
                ret = -255;

            button->setExitCode(ret);
        }

        ui->la_custombuttons->addWidget(button);
    }
}

void MainWindow::permanentexit()
{
    QTimer::singleShot(250, qApp, SLOT(quit()));
}

void MainWindow::readstdin()
{
    int dec;
    bool ok;

    QTextStream file(stdin);

    while (!file.atEnd())
    {
        QString line = file.readLine();

        if (line == "\\n")
        {
            qDebug() << "I want to break free";
            break;
        }

        dec = line.toInt(&ok, 10);
        if (ok)
            ui->progressBar->setValue(dec);
        else
            ui->label->setText(line);
    }

    emit finishReadStdin();
}

void MainWindow::on_bb_default_accepted()
{
    if (!ui->lineEdit->isHidden())
        utils::qStdOut() << ui->lineEdit->text() << endl;

    if (!ui->la_slider_2->isHidden())
        utils::qStdOut() << ui->horizontalSlider->value() << endl;

    qApp->exit(0);
}

void MainWindow::on_bb_default_rejected()
{
    qApp->exit(1);
}

void MainWindow::on_btn_close_clicked()
{
    if (!ui->lineEdit->isHidden())
        utils::qStdOut() << ui->lineEdit->text() << endl;

    if (!ui->la_slider_2->isHidden())
        utils::qStdOut() << ui->horizontalSlider->value() << endl;

    qApp->exit(0);
}
