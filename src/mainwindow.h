#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QApplication>
#include <QFile>
#include <QtConcurrent/qtconcurrentrun.h>
#include <QDesktopWidget>

#include "utils.h"
#include "fbutton.h"

namespace Ui {class MainWindow;}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = 0);
        ~MainWindow();

        void Progressbar();
        void AutoClose();
        void NoCancel();
        void SetTitle(const QString &title);
        void SetText(const QString &text);
        void AddCustomButtons(const QStringList &buttons);
        void ScaleDialog();
        void EntryDialog();

        void setupWindow(int resize, bool centered = false);

    signals:
        void finishReadStdin();

    private slots:
        void on_bb_default_accepted();
        void on_bb_default_rejected();
        void on_btn_close_clicked();
        void sliderValueChange(int value);

        void permanentexit();

    private:
        Ui::MainWindow *ui;

        void readstdin();

        bool autoclose = false;
        bool nocancel = false;
};

#endif // MAINWINDOW_H
