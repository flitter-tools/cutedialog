#ifndef UTILS_H
#define UTILS_H

#include <QTextStream>

class utils
{
    public:
        static QTextStream& qStdOut();

    private:
        utils(){}
};

#endif // UTILS_H
