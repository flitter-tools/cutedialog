#include "mainwindow.h"
#include "fcolordialog.h"

#include <QApplication>
#include <QCommandLineParser>
#include <QColorDialog>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QCommandLineParser parser;
    parser.setApplicationDescription("Yet another another another .... gui dialog. "
                                     "Still it can less then yad or so, because writen to be simple as "
                                     "possible and origignal idea to replace gtk dialog for steam. "
                                     "It has same parametrs as zenity or yad, so it has minimal conflict issue.");

    parser.addHelpOption();
    parser.addVersionOption();
    parser.addOption(QCommandLineOption("text", "Display text label. Duh.", "text"));
    parser.addOption(QCommandLineOption("width", "Set width of the dialog.", "num"));
    parser.addOption(QCommandLineOption("no-cancel", "Remove close button. Only works with progress option."));
    parser.addOption(QCommandLineOption("auto-close", "Automatically close dialog when task is done."));
    parser.addOption(QCommandLineOption("progress", "Display progressbar. Progress % must be piped."));
    parser.addOption(QCommandLineOption({"color", "show-palette", "color-selection"}, "Display color dialog."));
    parser.addOption(QCommandLineOption("title", "Set title of dialog.", "text", "foo bar"));
    parser.addOption(QCommandLineOption("center", "Display dialog in center of screen."));
    parser.addOption(QCommandLineOption("button", "Add custom button to dialog.", "text:num"));
    parser.addOption(QCommandLineOption("scale", "Display scale dialog."));
    parser.addOption(QCommandLineOption("entry", "Display text editing dialog."));
    parser.process(a);

    if (parser.isSet("color"))
    {
        fcolordialog cdlg;
        cdlg.show();
        return cdlg.exec();
    }
    else
    {
        MainWindow w;

        if (parser.isSet("title"))
            w.SetTitle(parser.value("title"));

        if (parser.isSet("progress"))
        {
            if (parser.isSet("no-cancel"))
                w.NoCancel();
            if (parser.isSet("auto-close"))
                w.AutoClose();
            w.Progressbar();
        }

        if (parser.isSet("button"))
            w.AddCustomButtons(parser.values("button"));

        if (parser.isSet("text"))
            w.SetText(parser.value("text"));

        int resize = 0;
        if (parser.isSet("width"))
            resize = parser.value("width").toInt();

        if (parser.isSet("scale"))
            w.ScaleDialog();

        if (parser.isSet("entry"))
            w.EntryDialog();

        w.setupWindow(resize, parser.isSet("center"));
        w.show();
        return a.exec();
    }
}
