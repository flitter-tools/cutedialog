#include "fcolordialog.h"

fcolordialog::fcolordialog()
{
    connect(this, &fcolordialog::accepted, this, &fcolordialog::execi);
}

void fcolordialog::execi()
{
    utils::qStdOut() << this->selectedColor().name() << endl;
}
