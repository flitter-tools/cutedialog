#ifndef FBUTTON_H
#define FBUTTON_H

#include <QPushButton>
#include <QApplication>

class fbutton : public QPushButton
{
    //Q_OBJECT

    public:
        explicit fbutton(const QString &label);
        virtual ~fbutton(){}

        void setExitCode(const int &code);

    private slots:
        void fexit();

    private:
        int code = 0;
};

#endif // FBUTTON_H
