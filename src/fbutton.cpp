#include "fbutton.h"

fbutton::fbutton(const QString &label)
{
    this->setText(label);

    connect(this, &fbutton::clicked, this, &fbutton::fexit);
}

void fbutton::setExitCode(const int &code)
{
    this->code = code;
}

void fbutton::fexit()
{
    qApp->exit(code);
}
